package com.example.preexamen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

public class ImcActivity extends AppCompatActivity {
    private TextView txtNombre;
    private EditText txtMetro;
    private EditText txtCm;
    private EditText txtPeso;
    private TextView lblResultadoImc;
    private Button btnCalcularImc;
    private Button btnLimpiarImc;
    private Button btnRegresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imcactivity);
        this.txtNombre = (TextView) findViewById(R.id.lblNombreImc);
        this.txtMetro = (EditText) findViewById(R.id.txtMetro);
        this.txtCm = (EditText) findViewById(R.id.txtCm);
        this.txtPeso = (EditText) findViewById(R.id.txtPeso);
        this.btnCalcularImc = (Button) findViewById(R.id.btnCalcularImc);
        this.btnLimpiarImc = (Button) findViewById(R.id.btnLimpiarImc);
        this.btnRegresar = (Button) findViewById(R.id.btnRegresarImc);
        this.lblResultadoImc = (TextView) findViewById(R.id.lblResultadoImc);
        Bundle datos = getIntent().getExtras();
        this.txtNombre.setText("Bienvenido " + datos.get("nombre"));

        this.btnCalcularImc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!noCamposVacios()){
                    Toast.makeText(ImcActivity.this,"Complete todos los campos",Toast.LENGTH_SHORT).show();
                }else{
                    float metros = Float.parseFloat(txtMetro.getText().toString());
                    float cm = Float.parseFloat(txtCm.getText().toString())/100;
                    float peso = Float.parseFloat(txtPeso.getText().toString());
                    float altura = metros + cm;
                    DecimalFormat format = new DecimalFormat("#.0");
                    float resultado = peso / (altura * altura);
                    lblResultadoImc.setText(format.format(resultado));
                }
            }
        });

        this.btnLimpiarImc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtMetro.setText("");
                txtCm.setText("");
                txtPeso.setText("");
                lblResultadoImc.setText("");
            }
        });

        this.btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ImcActivity.this,MainActivity.class);
                finish();
                startActivity(intent);
            }
        });
    }

    public boolean noCamposVacios(){
        if(txtPeso.getText().toString().matches("") || txtCm.getText().toString().matches("") ||
        txtMetro.getText().toString().matches("")){
            return false;
        }else{
            return true;
        }
    }
}
