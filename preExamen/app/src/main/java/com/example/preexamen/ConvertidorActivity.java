package com.example.preexamen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.Format;

public class ConvertidorActivity extends AppCompatActivity {
    private TextView lblNombre;
    private EditText txtCantidad;
    private RadioButton rbtnCelFah;
    private RadioButton rbtnFahCel;
    private TextView lblResultado;
    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnRegresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_convertidor);
        this.lblNombre = (TextView) findViewById(R.id.lblNombre);
        Bundle datos = getIntent().getExtras();
        this.lblNombre.setText("Bienvenido " + datos.get("nombre"));
        this.lblResultado = (TextView) findViewById(R.id.lblResultado);
        this.txtCantidad = (EditText) findViewById(R.id.txtCantidad);
        this.rbtnCelFah = (RadioButton) findViewById(R.id.rbtnCelFah);
        this.rbtnFahCel = (RadioButton) findViewById(R.id.rbtnFahCel);
        this.btnCalcular = (Button) findViewById(R.id.btnCalcular);
        this.btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        this.btnRegresar = (Button) findViewById(R.id.btnRegresar);

        this.btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtCantidad.getText().toString().matches("") || txtCantidad.getText().toString().isEmpty()){
                    Toast.makeText(ConvertidorActivity.this,"Ingrese una cantidad",Toast.LENGTH_SHORT).show();
                }else{
                    float grados = Float.parseFloat(txtCantidad.getText().toString());
                    Format format = new DecimalFormat("#.0");
                    System.out.println(grados);
                    if(rbtnCelFah.isChecked()){
                        float resultado = ((9*grados)/5) + 32;
                        lblResultado.setText(format.format(resultado) + "°F");
                    }else if(rbtnFahCel.isChecked()){
                        float resultadoF = ((grados - 32) * 5)/9;
                        System.out.println(resultadoF);
                        lblResultado.setText(format.format(resultadoF) + "°C");
                    }
                }
            }
        });

        this.btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtCantidad.setText("");
                rbtnCelFah.setChecked(true);
                rbtnFahCel.setChecked(false);
                lblResultado.setText("");
            }
        });

        this.btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ConvertidorActivity.this,MainActivity.class);
                finish();
                startActivity(intent);
            }
        });
    }
}
