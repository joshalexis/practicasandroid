package com.example.preexamen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText txtNombre;
    private Button btnIMC;
    private Button btnConvertidor;
    private Button btnTerminar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.txtNombre = (EditText) findViewById(R.id.txtNombre);
        this.btnIMC = (Button) findViewById(R.id.btnIMC);
        this.btnConvertidor = (Button) findViewById(R.id.btnConvertidor);
        this.btnTerminar = (Button) findViewById(R.id.btnTerminar);

        this.btnIMC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nombre = txtNombre.getText().toString();
                if(nombre.matches("") || nombre.isEmpty()){
                    Toast.makeText(MainActivity.this,"Ingrese un nombre",Toast.LENGTH_SHORT).show();
                }else{
                    Intent intent = new Intent(MainActivity.this, ImcActivity.class);
                    intent.putExtra("nombre",nombre);
                    finish();
                    startActivity(intent);
                }
            }
        });

        this.btnConvertidor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nombre = txtNombre.getText().toString();
                if(nombre.isEmpty() || nombre.matches("")){
                    Toast.makeText(MainActivity.this,"Ingrese un nombre",Toast.LENGTH_SHORT).show();
                }else{
                    Intent intent = new Intent(MainActivity.this,ConvertidorActivity.class);
                    intent.putExtra("nombre",nombre);
                    finish();
                    startActivity(intent);
                }
            }
        });

        this.btnTerminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
