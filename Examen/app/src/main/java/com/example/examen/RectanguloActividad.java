package com.example.examen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.Format;

public class RectanguloActividad extends AppCompatActivity {
    private Rectangulo rectangulo;
    private TextView lblNombre;
    private EditText txtBase;
    private EditText txtAltura;
    private TextView lblCalculoArea;
    private TextView lblCalculoPerimetro;
    private TextView lblCalArea;
    private TextView lblCalPerimetro;
    private Button btnCalcular;
    private Button btnRegresar;
    private Button btnLimpiar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rectangulo_actividad);
        this.rectangulo = new Rectangulo();
        Bundle datos = getIntent().getExtras();
        this.lblNombre = (TextView) findViewById(R.id.lblNombre);
        this.lblNombre.setText("Mi nombre es " + datos.get("nombre"));
        this.txtBase = (EditText) findViewById(R.id.txtBase);
        this.txtAltura = (EditText) findViewById(R.id.txtAltura);
        this.lblCalculoArea = (TextView) findViewById(R.id.lblCalculoArea);
        this.lblCalculoPerimetro = (TextView)findViewById(R.id.lblCalculoPerimetro);
        this.lblCalArea = (TextView) findViewById(R.id.lblCalArea);
        this.lblCalPerimetro = (TextView)findViewById(R.id.lblCalPerimetro);
        this.btnCalcular = (Button) findViewById(R.id.btnCalcular);
        this.btnRegresar = (Button) findViewById(R.id.btnRegresar);
        this.btnLimpiar = (Button) findViewById(R.id.btnLimpiar);

        this.btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((txtBase.getText().toString().matches("") || txtAltura.getText().toString().matches("")) ||
                        (txtBase.getText().toString().matches("") && txtAltura.getText().toString().matches(""))){
                    Toast.makeText(RectanguloActividad.this,"Complete todo los campos",Toast.LENGTH_SHORT).show();
                }else{
                    Format format = new DecimalFormat("#.00");
                    rectangulo.setAltura(Integer.parseInt(txtAltura.getText().toString()));
                    rectangulo.setBase(Integer.parseInt(txtBase.getText().toString()));
                    lblCalculoArea.setText(format.format(rectangulo.calcularCalcularArea()));
                    lblCalculoPerimetro.setText(format.format(rectangulo.calcularPagPerimetro()));
                }
            }
        });

        this.btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        this.btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtAltura.setText("");
                txtBase.setText("");
                lblCalculoArea.setText("");
                lblCalculoPerimetro.setText("");
            }
        });
    }
}
