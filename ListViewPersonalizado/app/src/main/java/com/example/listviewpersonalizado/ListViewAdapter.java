package com.example.listviewpersonalizado;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class ListViewAdapter extends ArrayAdapter<ListViewItem> implements Filterable {
    int groupId;
    Activity Context;
    ArrayList<ListViewItem> list;
    ArrayList<ListViewItem> filterList;
    CustomFilter filter;
    LayoutInflater inflater;

    public ListViewAdapter(Activity Context,int groupId,ArrayList<ListViewItem> list){
        super(Context,groupId,list);
        this.list = list;
        this.inflater = (LayoutInflater) Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.groupId = groupId;
        this.filterList = list;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        View itemView = inflater.inflate(groupId,parent,false);
        ImageView imagen = (ImageView) itemView.findViewById(R.id.imgPais);
        imagen.setImageResource(list.get(position).getImagenID());
        TextView textNombrePais = (TextView) itemView.findViewById(R.id.lblnombrePais);
        textNombrePais.setText(list.get(position).getTextNombrePais());
        TextView textPIB = (TextView) itemView.findViewById(R.id.lblPIB);
        textPIB.setText(list.get(position).getTextPIB());
        TextView textPuestoMundial = (TextView) itemView.findViewById(R.id.lblPuesto);
        textPuestoMundial.setText(list.get(position).getTextPuestoMundial());
        return itemView;
    }

    @Override
    public int getCount(){
        return this.list.size();
    }

    @Override
    public ListViewItem getItem(int pos){
        return list.get(pos);
    }

    @Override
    public long getItemId(int pos){
        return list.indexOf(getItem(pos));
    }

    @Override
    public Filter getFilter(){
        if(filter == null){
            filter = new CustomFilter();
        }
        return filter;
}

    class CustomFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (constraint != null && constraint.length()>0){
                constraint = constraint.toString().toUpperCase();
                ArrayList<ListViewItem> filters = new ArrayList<ListViewItem>();

                for(ListViewItem itemList: filterList){
                    if(itemList.getTextNombrePais().toUpperCase().contains(constraint)){
                        filters.add(itemList);
                    }
                }
                results.count = filters.size();
                results.values = filters;
            }else{
                results.count = filterList.size();
                results.values = filterList;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            list = (ArrayList<ListViewItem>) results.values;
            notifyDataSetChanged();
        }
    }
}
