package com.example.listviewpersonalizado;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private ListView listView;
    private SearchView srcNombres;
    private ListViewAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final ArrayList<ListViewItem> list = new ArrayList<>();
        list.add(new ListViewItem(getString(R.string.paisArgentina),getString(R.string.pibArgentina),getString(R.string.puestoArgentina),
                R.drawable.argentina));
        list.add(new ListViewItem(getString(R.string.paisMexico),getString(R.string.pibMexico),getString(R.string.puestoMexico),
                R.drawable.mexico));
        list.add(new ListViewItem(getString(R.string.paisAustralia),getString(R.string.pibAustralia),getString(R.string.puestoAustralia),
                R.drawable.australia));
        list.add(new ListViewItem(getString(R.string.paisAlemania),getString(R.string.pibAlemania),getString(R.string.puestoAlemania),
                R.drawable.alemania));
        list.add(new ListViewItem(getString(R.string.paisEcuador),getString(R.string.pibEcuador),getString(R.string.puestoEcuador),
                R.drawable.ecuador));
        list.add(new ListViewItem(getString(R.string.paisEgipto),getString(R.string.pibEgipto),getString(R.string.puestoEgipto),
                R.drawable.egipto));
        list.add(new ListViewItem(getString(R.string.paisEspaña),getString(R.string.pibEspaña),getString(R.string.puestoEspaña),
                R.drawable.espana));
        list.add(new ListViewItem(getString(R.string.paisFrancia),getString(R.string.pibFrancia),getString(R.string.puestoFrancia),
                R.drawable.francia));
        list.add(new ListViewItem(getString(R.string.paisMarruecos),getString(R.string.pibMarruecos),getString(R.string.puestoMarruecos),
                R.drawable.marruecos));
        list.add(new ListViewItem(getString(R.string.paisMalasia),getString(R.string.pibMalasia),getString(R.string.puestoMalasia),
                R.drawable.malasia));

        //this.srcNombres = (SearchView) findViewById(R.id.menu_search);
        this.listView = (ListView) findViewById(R.id.lvPaises);
        adapter = new ListViewAdapter(MainActivity.this,R.layout.listview_layout,list);
        this.listView.setAdapter(adapter);
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(parent.getContext(),"Ha seleccionado " +
                                ((ListViewItem) parent.getItemAtPosition(position)).getTextNombrePais(),
                        Toast.LENGTH_SHORT).show();
            }
        });


    }

    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.searchview,menu);

        MenuItem menuItem = menu.findItem(R.id.menu_search);
        SearchView searchView = (SearchView) menuItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

}
