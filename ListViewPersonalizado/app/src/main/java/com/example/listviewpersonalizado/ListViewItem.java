package com.example.listviewpersonalizado;

public class ListViewItem {
    private String textNombrePais;
    private String textPIB;
    private String textPuestoMundial;
    private Integer imagenID;

    public ListViewItem(String textNombrePais, String textPIB, String textPuestoMundial, Integer imagenID) {
        this.textNombrePais = textNombrePais;
        this.textPIB = textPIB;
        this.textPuestoMundial = textPuestoMundial;
        this.imagenID = imagenID;
    }

    public String getTextNombrePais() {
        return textNombrePais;
    }

    public void setTextNombrePais(String textNombrePais) {
        this.textNombrePais = textNombrePais;
    }

    public String getTextPIB() {
        return textPIB;
    }

    public void setTextPIB(String textPIB) {
        this.textPIB = textPIB;
    }

    public String getTextPuestoMundial() {
        return textPuestoMundial;
    }

    public void setTextPuestoMundial(String textPuestoMundial) {
        this.textPuestoMundial = textPuestoMundial;
    }

    public Integer getImagenID() {
        return imagenID;
    }

    public void setImagenID(Integer imagenID) {
        this.imagenID = imagenID;
    }
}
