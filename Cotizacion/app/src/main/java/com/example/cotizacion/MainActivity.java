package com.example.cotizacion;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Cotizacion cotizacion;
    private EditText txtCliente;
    private Button btnIr;
    private Button btnSalir;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.txtCliente = (EditText) findViewById(R.id.txtCliente);
        this.btnIr = (Button) findViewById(R.id.btnIr);
        this.btnSalir = (Button) findViewById(R.id.btnSalir);
        this.cotizacion = new Cotizacion();

        this.btnIr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cliente = txtCliente.getText().toString();
                if(cliente.matches("") || cliente.isEmpty() ){
                    Toast.makeText(MainActivity.this,"Faltó capturar un nombre",Toast.LENGTH_SHORT).show();
                }else{
                    Intent intent = new Intent(MainActivity.this,CotizacionActivity.class);
                    //Enviar un dato String
                    intent.putExtra("cliente",cliente);
                    //Enviar un objeto
                    Bundle objeto = new Bundle();
                    objeto.putSerializable("cotizacion",cotizacion);
                    intent.putExtras(objeto);
                    finish();
                    startActivity(intent);
                }
            }
        });
        this.btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
