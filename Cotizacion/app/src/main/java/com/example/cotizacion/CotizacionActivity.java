package com.example.cotizacion;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class CotizacionActivity extends AppCompatActivity {
    private Cotizacion cotizacion;
    private TextView lblNombre;
    private TextView lblFolio;
    private EditText txtDescripcion;
    private EditText txtValor;
    private EditText txtPorEnganche;
    private RadioButton radioButton12;
    private RadioButton radioButton24;
    private RadioButton radioButton36;
    private RadioButton radioButton48;
    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnRegresar;
    private TextView lblPagoMensual;
    private TextView lblEnganche;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cotizacion);
        this.lblFolio = (TextView) findViewById(R.id.lblFolio);
        this.lblNombre = (TextView) findViewById(R.id.lblNombre);
        Bundle datos = getIntent().getExtras();
        this.lblNombre.setText("Cliente: " + datos.getString("cliente"));
        this.cotizacion = (Cotizacion) datos.getSerializable("cotizacion");
        String folio = String.valueOf(this.cotizacion.getFolio());
        this.lblFolio.setText("Folio: " + folio);
        this.txtDescripcion = (EditText) findViewById(R.id.txtDescripcion);
        this.txtValor = (EditText) findViewById(R.id.txtValor);
        this.txtPorEnganche = (EditText) findViewById(R.id.txtPorEnganche);
        this.radioButton12 = (RadioButton) findViewById(R.id.rbtn12);
        this.radioButton24 = (RadioButton) findViewById(R.id.rbtn18);
        this.radioButton36 = (RadioButton) findViewById(R.id.rbtn24);
        this.radioButton48 = (RadioButton) findViewById(R.id.rbtn36);
        this.btnCalcular = (Button) findViewById(R.id.btnCalcular);
        this.btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        this.btnRegresar = (Button) findViewById(R.id.btnRegresar);
        this.lblPagoMensual = (TextView) findViewById(R.id.lblPagoMensual);
        this.lblEnganche = (TextView) findViewById(R.id.lblEnganche);

        this.btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtDescripcion.getText().toString().matches("") || txtPorEnganche.getText().toString().matches("")
                        || txtValor.getText().toString().matches("")){
                    Toast.makeText(CotizacionActivity.this,"Llene todos los campos",Toast.LENGTH_SHORT).show();
                }else{
                    cotizacion.setDescripcion(txtDescripcion.getText().toString());
                    cotizacion.setValorAuto(Float.parseFloat(txtValor.getText().toString()));
                    cotizacion.setPorEnganche(Float.parseFloat(txtPorEnganche.getText().toString()));
                    if(radioButton12.isChecked()){
                        cotizacion.setPlazos(12);
                    }else if(radioButton24.isChecked()){
                        cotizacion.setPlazos(18);
                    }else if(radioButton36.isChecked()){
                        cotizacion.setPlazos(24);
                    }else if(radioButton48.isChecked()){
                        cotizacion.setPlazos(36);
                    }
                    lblPagoMensual.setText("Pago mensual es: " + cotizacion.calcularPagoMensual());
                    lblEnganche.setText("Enganche: " + cotizacion.calcularEnganche());
                }
            }
        });
        this.btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtValor.setText("");
                txtPorEnganche.setText("");
                txtDescripcion.setText("");
                lblEnganche.setText("Enganche:");
                lblPagoMensual.setText("Pago mensual es:");
                radioButton12.setChecked(true);
                radioButton24.setChecked(false);
                radioButton36.setChecked(false);
                radioButton48.setChecked(false);
            }
        });
        this.btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CotizacionActivity.this,MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
