package com.example.conversormoneda;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;


public class MainActivity extends AppCompatActivity {
    private EditText txtPesos;
    private TextView lblResultado;
    private Spinner spnDivisas;
    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnCerrar;
    private int divisa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.txtPesos = (EditText) findViewById(R.id.txtPesos);
        this.lblResultado = (TextView) findViewById(R.id.lblResultado);
        this.spnDivisas = (Spinner) findViewById(R.id.spnDivisas);
        this.btnCalcular = (Button) findViewById(R.id.btnCalcular);
        this.btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        this.btnCerrar = (Button) findViewById(R.id.btnCerrar);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(MainActivity.this,android.R.layout.simple_expandable_list_item_1,
                getResources().getStringArray(R.array.monedas));
        this.spnDivisas.setAdapter(adapter);
        spnDivisas.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                divisa = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        this.btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtPesos.getText().toString().matches("")){
                    Toast.makeText(MainActivity.this, "Ingrese pesos a cambiar",Toast.LENGTH_SHORT).show();
                }else{
                    double pesos = Double.parseDouble(txtPesos.getText().toString());
                    double resultado;
                    //canada 0,069990
                    //euro 0,048290
                    //dolar 0,053570
                    //libra 0,041160
                    DecimalFormat format = new DecimalFormat("#.00");
                    if(divisa == 0){
                        resultado = pesos * 0.053570;
                        lblResultado.setText("Total: $" + format.format(resultado));
                    }else if(divisa == 1){
                        resultado = pesos * 0.048290;
                        lblResultado.setText("Total: $" + format.format(resultado));
                    }else if(divisa == 2){
                        resultado = pesos * 0.069990;
                        lblResultado.setText("Total: $" + format.format(resultado));
                    }else if(divisa == 3){
                        resultado = pesos * 0.041160;
                        lblResultado.setText("Total: $" + format.format(resultado));
                    }
                }
            }
        });
        this.btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtPesos.setText("");
                lblResultado.setText("Total: $");
                spnDivisas.setSelection(0);
            }
        });
        this.btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
