package com.example.spinnerpersonalizado;

public class ItemData {
    private String textCategoria;
    private String textDescripcion;
    private Integer imageId;

    public ItemData(String textCategoria, String textDescripcion, Integer imageId){
        this.textCategoria = textCategoria;
        this.textDescripcion = textDescripcion;
        this.imageId = imageId;
    }

    public String getTextCategoria(){
        return this.textCategoria;
    }

    public void setTextCategoria(String textCategoria){
        this.textCategoria = textCategoria;
    }

    public String getTextDescripcion(){
        return this.textDescripcion;
    }

    public void setTextDescripcion(String textDescripcion){
        this.textCategoria = textDescripcion;
    }

    public Integer getImageId(){
        return this.imageId;
    }

    public void setImageId(Integer imageId){
        this.imageId = imageId;
    }
}
